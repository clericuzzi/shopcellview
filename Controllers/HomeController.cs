using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ShopCell.Models;
using ShopCell.Utils;

namespace ShopCell.Controllers
{
    public class HomeController : Controller
    {
        private MyBIContext _context;
        public HomeController(MyBIContext context)
        {   
            _context = context;
        }

        public IActionResult Index()
        {
            string query = UtilQuery.QUERY;
            query = query.Replace(":datainicial", "null").Replace(":datafinal", "null");

            List<ReportData> models = _context.DisplayTypes.FromSql(query).ToList();
            ViewData["models"] = models;
            
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
