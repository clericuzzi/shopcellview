namespace ShopCell.Utils
{
    public static class UtilQuery
    {
        public static string QUERY = @"SELECT 
 -- f.idfilial,
  f.numerofilial,
  f.fantasia,
  COALESCE(ven.vendabruta,0) - COALESCE(dev.vendabruta,0) AS vendaliquida,
  COALESCE(serv.vendaservico,0) -  COALESCE(devserv.vendaservico,0) AS Servico,  
  (COALESCE(ven.total_desconto,0) - COALESCE(dev.total_desconto,0)) * -1 AS desconto,
  (COALESCE(ven.vendabruta,0) - COALESCE(dev.vendabruta,0)) - (COALESCE(ven.totalpresente,0) - COALESCE(dev.totalpresente,0)) AS juros,
  COALESCE(ven.totalprecocusto,0) - COALESCE(dev.totalprecocusto,0) AS custoilka,
  COALESCE(ven.custoreal,0) - COALESCE(dev.custoreal,0) AS custoreal,
  (CASE WHEN ((ven.vendabruta = 0) OR (ven.custoreal IS NULL) OR (ven.vendabruta IS NULL))
     THEN 0
    ELSE ROUND((100 -((COALESCE(ven.custoreal,0) - COALESCE(dev.custoreal,0)) / (COALESCE(ven.vendabruta,0) - COALESCE(dev.vendabruta,0)) * 100)), 2)
        END) AS recustoreal


  FROM glb.filial f LEFT JOIN  
                              (SELECT 
                                ib.idfilial,
                                SUM(ib.totalitem) vendabruta,
                                SUM(ib.totaldesejadopresente) AS totaldesejadopresente,
                                SUM(ib.totaldesejadopresente) - SUM(ib.totalpresente) AS total_desconto,
                                SUM(ib.totalpresente) AS totalpresente,
                                SUM(ib.totalprecocusto) AS totalprecocusto,
                                SUM(ib.totalcustomedio) AS totalcustomedio,
                                SUM(ib.quantidade * COALESCE(pgf.custoreposicao,pgg.custoreposicao)) AS custoreposicao,
                                SUM(CASE WHEN  SUBSTRING(dep.classificacao FROM 1 FOR 2) = '08' THEN  ROUND(ib.totalprecocusto -  (ib.totalprecocusto *0.0909),2)
                                     WHEN SUBSTRING(dep.classificacao FROM 1 FOR 2) = '13' THEN ROUND(ib.totalprecocusto -  (ib.totalprecocusto *0.0909),2)
                                 ELSE   ROUND(ib.totalprecocusto -  (ib.totalprecocusto * 0.1304),2)END) AS custoreal
                               FROM rst.itembase ib INNER JOIN glb.produto p             ON p.idproduto = ib.idproduto
                                                    LEFT JOIN glb.departamento dep       ON dep.iddepartamento = p.iddepartamento
                                                    LEFT JOIN rst.produtogradefilial pgf ON pgf.idfilial = 10006 AND
                                                                                            ib.idproduto = pgf.idproduto AND
                                                                                            ib.idgradex = pgf.idgradex AND
                                                                                            ib.idgradey = pgf.idgradey
                                                    LEFT JOIN rst.produtogradefilial pgg ON pgg.idfilial = 10001 AND
                                                                                            ib.idproduto = pgg.idproduto AND
                                                                                            ib.idgradex = pgg.idgradex AND
                                                                                            ib.idgradey = pgg.idgradey                                        
                                WHERE ib.datamovimento BETWEEN :datainicial AND :datafinal
                                 AND ib.idoperacaoproduto = 102010
                                 
                              GROUP BY 1) ven ON ven.idfilial = f.idfilial  
 

                    LEFT JOIN 

                              (SELECT 
                                ib.idfilial,
                                SUM(ib.totalitem) vendabruta,
                                SUM(ib.totaldesejadopresente) AS totaldesejadopresente,
                                SUM(ib.totaldesejadopresente) - SUM(ib.totalpresente) AS total_desconto,
                                SUM(ib.totalpresente) AS totalpresente,
                                SUM(ib.totalprecocusto) AS totalprecocusto,
                                SUM(ib.totalcustomedio) AS totalcustomedio,
                                SUM(ib.quantidade * COALESCE(pgf.custoreposicao,pgg.custoreposicao)) AS custoreposicao,
                                SUM(CASE WHEN  SUBSTRING(dep.classificacao FROM 1 FOR 2) = '08' THEN  ROUND(ib.totalprecocusto -  (ib.totalprecocusto *0.0909),2)
                                     WHEN SUBSTRING(dep.classificacao FROM 1 FOR 2) = '13' THEN ROUND(ib.totalprecocusto -  (ib.totalprecocusto *0.0909),2)
                                 ELSE   ROUND(ib.totalprecocusto -  (ib.totalprecocusto * 0.1304),2)END) AS custoreal
                                

                               FROM rst.itembase ib INNER JOIN glb.produto p             ON p.idproduto = ib.idproduto
                                                    LEFT JOIN glb.departamento dep       ON dep.iddepartamento = p.iddepartamento
                                                    LEFT JOIN rst.produtogradefilial pgf ON pgf.idfilial = 10006 AND
                                                                                            ib.idproduto = pgf.idproduto AND
                                                                                            ib.idgradex = pgf.idgradex AND
                                                                                            ib.idgradey = pgf.idgradey
                                                    LEFT JOIN rst.produtogradefilial pgg ON pgg.idfilial = 10001 AND
                                                                                            ib.idproduto = pgg.idproduto AND
                                                                                            ib.idgradex = pgg.idgradex AND
                                                                                            ib.idgradey = pgg.idgradey                                                                                
                               WHERE ib.datamovimento BETWEEN :datainicial AND :datafinal
                                 AND ib.idoperacaoproduto = 101040
                                 
                              GROUP BY 1) AS dev ON dev.idfilial = f.idfilial
                              
                    LEFT JOIN 

                              (SELECT 
                                nt.idfilial,
                                SUM(ni.valorimposto) AS valorimposto

                                

                               FROM rst.nota nt INNER JOIN rst.notaimposto ni ON nt.idfilial = ni.idfilial AND
                                                                                 nt.idregistronota = ni.idregistronota
                               WHERE nt.datamovimento BETWEEN :datainicial AND :datafinal
                                 AND nt.idprocessomestre = 9081
                                 AND ni.idtipoimposto = 9
                                 
                              GROUP BY 1) AS st ON st.idfilial = f.idfilial

     LEFT JOIN 
     
(SELECT 
  its.idfilial,
  ROUND(SUM(its.repasse), 2)  AS custoservico,    
  ROUND(SUM(its.totalfuturo), 2)  AS vendaservico
    
 FROM rst.itemservico its LEFT JOIN glb.servico  srv ON its.idservico  = srv.idservico 
                          LEFT JOIN glb.gruposervico gse ON gse.idgruposervico = srv.idgruposervico
               
                                          
   WHERE its.datamovimento BETWEEN :datainicial AND :datafinal
     AND its.idoperacaoservico = 202010 -- Venda
GROUP BY 1) AS serv ON serv.idfilial = f.idfilial
  

     LEFT JOIN 
     
(SELECT 
  its.idfilial,
  ROUND(SUM(its.repasse), 2)  AS custoservico,    
  ROUND(SUM(its.totalfuturo), 2)  AS vendaservico
  
 FROM rst.itemservico its LEFT JOIN glb.servico  srv ON its.idservico  = srv.idservico 
                          LEFT JOIN glb.gruposervico gse ON gse.idgruposervico = srv.idgruposervico
               
                                          
   WHERE its.datamovimento BETWEEN :datainicial AND :datafinal
     AND its.idoperacaoservico = 201020 -- dev serv
     
GROUP BY 1) AS devserv ON devserv.idfilial = f.idfilial
  


WHERE f.idfilial NOT IN (10003,10006, 10007,10009,10012,10018,10019,10020,10900,10901)";
    }
}