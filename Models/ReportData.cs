using System;
using System.ComponentModel.DataAnnotations;

namespace ShopCell.Models
{
    public class ReportData 
    {
        public string NumeroFilial { get; set; }
        public string Fantasia { get; set; }
        public string Vendaliquida { get; set; }
        public string Servico { get; set; }
        public string Desconto { get; set; }
        public string Juros { get; set; }
        public string CustoIlka { get; set; }
        public string CustoReal { get; set; }
        public string RecustoReal { get; set; }
    }
}