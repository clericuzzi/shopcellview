using Microsoft.EntityFrameworkCore;

namespace ShopCell.Models
{
    public class MyBIContext : DbContext 
    {
        public MyBIContext (DbContextOptions<MyBIContext> options) : base (options)
        {            
        }

        public DbSet<ReportData> DisplayTypes { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ReportData>().HasKey(k => new { k.NumeroFilial, k.Fantasia });
        }
    }
}